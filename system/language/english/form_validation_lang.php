<?php

$lang['required']			= "Le champ '%s' est nécessaire.";
$lang['isset']				= "Le champ '%s' doit avoir une valeur.";
$lang['valid_email']		= "Le champ '%s' doit contenir une adresse mail valide.";
$lang['valid_emails']		= "Le champ '%s' doit contain all valid email addresses.";
$lang['valid_url']			= "Le champ '%s' doit contain a valid URL.";
$lang['valid_ip']			= "Le champ '%s' doit contain a valid IP.";
$lang['min_length']			= "Le champ '%s' doit avoir au moins %s caractères.";
$lang['max_length']			= "Le champ '%s' ne peut pas avoir plus de %s caractères.";
$lang['exact_length']		= "Le champ '%s' doit be exactly %s characters in length.";
$lang['alpha']				= "Le champ '%s' may only contain alphabetical characters.";
$lang['alpha_numeric']		= "Le champ '%s' may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "Le champ '%s' may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "Le champ '%s' doit contain only numbers.";
$lang['is_numeric']			= "Le champ '%s' doit contain only numeric characters.";
$lang['integer']			= "Le champ '%s' doit contain an integer.";
$lang['regex_match']		= "Le champ '%s' is not in the correct format.";
$lang['matches']			= "Le champ '%s' does not match the %s field.";
$lang['is_unique'] 			= "Le champ '%s' doit contain a unique value.";
$lang['is_natural']			= "Le champ '%s' doit contenir que des chiffres.";
$lang['is_natural_no_zero']	= "Le champ '%s' doit contain a number greater than zero.";
$lang['decimal']			= "Le champ '%s' doit contain a decimal number.";
$lang['less_than']			= "Le champ '%s' doit contain a number less than %s.";
$lang['greater_than']		= "Le champ '%s' doit contain a number greater than %s.";

// $lang['required']			= "The %s field is required.";
// $lang['isset']				= "The %s field must have a value.";
// $lang['valid_email']		= "The %s field must contain a valid email address.";
// $lang['valid_emails']		= "The %s field must contain all valid email addresses.";
// $lang['valid_url']			= "The %s field must contain a valid URL.";
// $lang['valid_ip']			= "The %s field must contain a valid IP.";
// $lang['min_length']			= "The %s field must be at least %s characters in length.";
// $lang['max_length']			= "The %s field can not exceed %s characters in length.";
// $lang['exact_length']		= "The %s field must be exactly %s characters in length.";
// $lang['alpha']				= "The %s field may only contain alphabetical characters.";
// $lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
// $lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
// $lang['numeric']			= "The %s field must contain only numbers.";
// $lang['is_numeric']			= "The %s field must contain only numeric characters.";
// $lang['integer']			= "The %s field must contain an integer.";
// $lang['regex_match']		= "The %s field is not in the correct format.";
// $lang['matches']			= "The %s field does not match the %s field.";
// $lang['is_unique'] 			= "The %s field must contain a unique value.";
// $lang['is_natural']			= "The %s field must contain only positive numbers.";
// $lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
// $lang['decimal']			= "The %s field must contain a decimal number.";
// $lang['less_than']			= "The %s field must contain a number less than %s.";
// $lang['greater_than']		= "The %s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */