<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends app_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
		if (!$this->session->userdata('loggedin')){
			redirect('front/login');

		}
		$data = array();
		$user = $this->session->userdata('user');
		$this->data['user'] = $user;
		
	}

	public function index()
	{

		$bars = array();
		$this->load->model('Bar');
		$bars = $this->Bar->getBars();
		$data = array(
			'tab' => $bars
			);

		$this->load->view('back/header');
		$this->load->view('back/home_view', $data);
		//$this->load->view('back/addBar_view');
		$this->load->view('back/footer');
	}

	 public function logout(){
        $this->session->sess_destroy();
        redirect('/');
    }
}