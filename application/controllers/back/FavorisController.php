<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FavorisController extends app_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
		if (!$this->session->userdata('loggedin')){
			redirect('front/login');

		}
		$data = array();
		$user = $this->session->userdata('user');
		$this->data['user'] = $user;
		
	}

	public function all(){

		//ON récupère les checkin du bar
		$id = $this->session->userdata('user')->id_user;

		$this->load->model('Favoris');
		$favoris['bars'] = $this->Favoris->getFavorisByIdUser($id);
	
		$this->load_view_logged('favoris/all',$favoris);
		
	}
}