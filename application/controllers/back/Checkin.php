<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkin extends app_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
		if (!$this->session->userdata('loggedin')){
			redirect('front/login');

		}
		$data = array();
		$user = $this->session->userdata('user');
		$this->data['user'] = $user;
		
	}

	public function index(){

		//On récupère les valeurs envoyés en GET
		$idBar = $_GET['idBar'];
		$idUser = $_GET['idUser'];
	
			$data = array(
				'Bar_id_bar' => $idBar,
				'User_id_user' => $idUser,
				'date_passage' => date('Y-m-d H:i:s')	
			);

		//On les insère en base
			$this->load->model('Check');
			$this->Check->addCheckin($data);		
		
	}
	public function getChecksByIdInJSON($bar_id) {

		$this->load->model('Check');
		$checks = $this->Check->getCheckByIdBar($bar_id);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($checks));

	}
}