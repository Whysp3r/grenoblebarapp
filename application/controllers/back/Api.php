<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends app_Controller {

	

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
		if (!$this->session->userdata('loggedin')){
			show_error('Not allowed', 301);
		}		
	}

	public function index()
	{
	}

	public function bar(){
		$bars = $this->Bar->getBars();
		$this->output
   		 ->set_content_type('application/json')
   		 ->set_output(json_encode($bars));
	}

}