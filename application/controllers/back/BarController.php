<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BarController extends app_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
		if (!$this->session->userdata('loggedin')){
			redirect('front/login');

		}
		$data = array();
		$user = $this->session->userdata('user');
		$this->data['user'] = $user;
		
	}

	public function index(){
		$bars = array();
		$this->load->model('Bar');
		$bars = $this->Bar->getBars();
		$data = array(
			'tab' => $bars
		);
		$this->load_view_logged('home_view', $data);
	}

	public function addBar(){

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nom', 'Nom', 'required|max_length[20]|is_unique[bar.nom]|xss_clean');
		$this->form_validation->set_rules('adresse', 'Adresse', 'required|max_length[40]');
		$this->form_validation->set_rules('tel', 'Telephone', 'required|min_length[10]|max_length[10]|is_natural'); // number
		$this->form_validation->set_rules('ville', 'Ville', 'required|max_length[20]');

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

		//$this->form_validation->set_rules('email', 'Adresse email', 'required|valid_email');
		//$this->form_validation->set_rules('prenom', 'Pr&eacutenom', 'required');
		//$this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[20]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load_view_logged('addBar_view');
		}
		else
		{
			$data = array(
				'nom' => $this->input->post('nom'),
				'adresse' => $this->input->post('adresse'),
				'latitude' => $this->input->post('latitude'),
				'longitude' =>$this->input->post('longitude'),				
				'tel' => $this->input->post('tel'),
				'ville' => $this->input->post('ville')
				);			
			$this->Bar->addBar($data);
			redirect('back/home');
			echo("Bar ajout&eacute");
		}
	}

	public function search(){

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nom', 'Nom', 'required|max_length[20]');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load_view_logged('search_view');
		}
		else
		{
			$data = array(
				'nom' => $this->input->post('nom'),
			);	
			$res = $this->Bar->searchBarByName($data['nom']);
			$data =array(
				'bars'=>$res,
			);
			$this->load_view_logged('search_view',$data);
			//redirect('back/home');
		}
	}

	public function detailBar($id){

		//On recupère les donnés du bar
		$this->load->model('Bar');
		$bars = $this->Bar->getBarById($id);

		//ON récupère les checkin du bar
		$this->load->model('Check');
		$checkByID = $this->Check->getCheckByIdBar($id);
	
		$data =array(
			'bars'=>$bars,
			'checks'=>$checkByID
		);

		$this->load_view_logged('detailBar',$data);

	}

}