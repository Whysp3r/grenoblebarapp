<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends app_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('html');
	}

	public function index(){

		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));

		$this->form_validation->set_rules('email', 'Adresse email', 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_pass_check['.$this->input->post('email').']');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load_view('login_view');
        }
        else
        {
            $this->load->model('User');
            $mail=$this->input->post('email');
            $pass=$this->input->post('password');
            $user = $this->User->getUserByEmailAndPassword($mail,$pass);
            $data = array(
                'user'  => $user,
                'loggedin' => 'login'
            );
            $this->session->set_userdata($data);
            redirect('back/home');
        }
	}

	public function register(){

		$this->load->library('form_validation');
        $this->load->helper(array('url','form'));

		$this->form_validation->set_rules('email', 'Adresse email', 'required|valid_email|is_unique[user.mail]');
		$this->form_validation->set_rules('prenom', 'Prénom', 'required');
        $this->form_validation->set_rules('nom', 'Nom', 'required');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[20]|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

		if ($this->form_validation->run())
		{
			$data = array(
				'mail' => $this->input->post('email'),
				'nom' => $this->input->post('nom'),
                'prenom' => $this->input->post('prenom'),
				'password' => md5($this->input->post('password'))/// BUG md5 à voir
			);
			$this->User->register($data);
			$this->load->view('front/header');
			$this->load->view('front/register_success_view');
			$this->load->view('front/footer');
		}
		$this->load_view('register_view');
	}

	/* ------------------------------------------------------------------------------------------------------- */

	public function fbr(){

            //require APPLIBS.'Facebook'.DS.'GraphObject.php';
            $params = array('318859384936038','e87c083e5114819f87b5b6774afbc330');
            // require(APPPATH.'libraries/gbafacebook/GraphObject.php');
            // require(APPPATH.'libraries/gbafacebook/GraphUser.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookSDKException.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookRequestException.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookAuthorizationException.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookOtherException.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookResponse.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookRequest.php');
            // require(APPPATH.'libraries/gbafacebook/FacebookSession.php');
            //require(APPPATH.'libraries/gbafacebook/FacebookJavaScriptLoginHelper.php');

            // $this->load->library('FacebookSession', $params);
            // FacebookSession::setDefaultApplication('318859384936038', 'e87c083e5114819f87b5b6774afbc330');

            // $this->load->library('GraphObject', $params);
            // $this->load->library('GraphUser');
            // $this->load->library('FacebookSDKException');
            // $this->load->library('FacebookRequestException');
            // $this->load->library('FacebookAuthorizationException');
            // $this->load->library('FacebookOtherException');
            // $this->load->library('FacebookResponse');
            // $this->load->library('FacebookRequest');
            // $this->load->library('FacebookJavaScriptLoginHelper');

            
            // $this->load->library('FacebookRequest');
            // $this->load->library('GraphUser');
            // $this->load->library('FacebookRequestException');

            require(APPPATH.'libraries/FacebookRequest.php');
            require(APPPATH.'libraries/GraphObject.php');
            require(APPPATH.'libraries/GraphUser.php');
            require(APPPATH.'libraries/FacebookSDKException.php');
            require(APPPATH.'libraries/FacebookResponse.php');
            require(APPPATH.'libraries/FacebookRequestException.php');
            require(APPPATH.'libraries/FacebookAuthorizationException.php');
            require(APPPATH.'libraries/FacebookOtherException.php');
            require(APPPATH.'libraries/FacebookSession.php');
            require(APPPATH.'libraries/FacebookJavaScriptLoginHelper.php');

            FacebookSession::setDefaultApplication('318859384936038', 'e87c083e5114819f87b5b6774afbc330');

            // $helper = new FacebookJavaScriptLoginHelper();
            $FacebookSession = new FacebookSession($params);
            $helper = new FacebookJavaScriptLoginHelper('318859384936038');
            $session = $helper->getSession();
            try {
              $me = (new FacebookRequest(
                $session, 'GET', '/me'
              ))->execute()->getGraphObject(GraphUser::className());

            } catch (FacebookRequestException $e) {
                $this->Session->setFlash(__('Une erreur est survenue avec la connexion facebook, veuillez réessayer ultérieurement'), 'flash_err');
                return $this->redirect(array('action' => 'login'));
            } catch (\Exception $e) {
                $this->Session->setFlash(__('Une erreur est survenue avec la connexion facebook, veuillez réessayer ultérieurement'), 'flash_err');
                return $this->redirect(array('action' => 'login'));
            }
            

            $user = array(
                'User'=> array(
                    'facebook_id' =>  $me->getProperty('id'),
                    'email'=> $me->getProperty('email'),
                )
            );
            // 12/24/1992
            //1992-12-24
            $temp = new datetime($me->getProperty('birthday'));
            $temp = date_format($temp, 'Y-m-d');
            //debug($temp);
            //die();
            $profil = array(
                'Profile' => array(
                    'first_name'=> $me->getProperty('first_name'),
                    'gender'=> $me->getProperty('gender'), //female
                    'last_name'=> $me->getProperty('last_name'),
                    'country'=> $me->getProperty('locale'),
                    'birthday'=> $temp,
                )
            ); 



            //////////////////////////////////////////////////////////////////////////////////////////
            //getUserByEmail
            var_dump($me);
            $mail = $me->getProperty('email');
            $fbid = $me->getProperty('id');
            $user = $this->User->getUserByEmail($mail);
            if($user){
                // Il existe déja
                $user = $this->User->getUserByFacebookid($fbid);
                $data = array(
                       'user'  => $user,
                       'loggedin' => 'login'
                   );
                $this->session->set_userdata($data);
                redirect('/back/home');
            } else {
                // Il n'existe pas
                $data = array(
                    'facebook_id' => $fbid,
                    'mail' => $mail,
                    'nom' => $me->getProperty('last_name'),
                    'prenom' => $me->getProperty('first_name'),
                );
                $this->User->register($data);
                $user = $this->User->getUserByFacebookid($fbid);
                $data = array(
                       'user'  => $user,
                       'loggedin' => 'login'
                   );
                $this->session->set_userdata($data);
                redirect('back/home');
            }
        }

        /* --------------------------------------------------------------------------------------------- */

	public function email_check($str){
		$login = $this->User->getUserByEmail($str);

		if ($login == FALSE ){
			$this->form_validation->set_message('email_check', 'The email address does not exist');
			return FALSE;
		}
		else
		{
			return true;
		}
		

	}

    public function tom(){
        $user = $this->User->getUserByFacebookid('2147483647');
        var_dump($user);
        die();
    }

	public function pass_check($str, $mail){ //CELA NE FONCTIONNE PAS

		$pass = $this->User->getbyEmailPassword($mail, $str);

		if ($pass == FALSE ){
			$this->form_validation->set_message('pass_check', 'The email and the password do not match');
			return FALSE;
		}
		else
		{
			return true;
		}
	}
}