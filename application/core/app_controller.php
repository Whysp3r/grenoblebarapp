<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 
class App_Controller extends CI_Controller  {

	function __construct(){
        parent::__construct();
    }

	public function load_view($view, $vars = array(), $vars1 = array(), $vars2 = array()) {
		if(isset($vars1))
			$vars1 = array('title'=>'Titre');

		$this->load->view('front/header', $vars1);
		$this->load->view('front/'.$view, $vars);
		$this->load->view('front/footer', $vars2);
	}

	public function load_view_logged($view, $vars = array(), $vars1 = array(), $vars2 = array()) {
		if(isset($vars1))
			$vars1 = array('title'=>'Titre');

		$this->load->view('back/header', $vars1);
		$this->load->view('back/'.$view, $vars);
		$this->load->view('back/footer', $vars2);
	}
}