<html>
<head>
	<?php
	if(!isset($title))
			$title = 'Titre';
	?>
	<title><?php echo $title ?> - OpenBarApp</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css">

	<style type="text/css">
	.container #bars_list {

	}
	.container ul a li span.btitle{
		display:inline-block;
		float:left;
		width:75%;
		height:45px;
		text-align: left;
		padding-left: 10px;
		overflow: hidden;
	}
	.container ul a li span.btitle:last-child{
		text-align: right;
		padding-left: 0;
		padding-right: 10px;
	}

	.container ul a li span {
		display: block;
		color: white;
		text-align: right;
		float:right;
		font-size:15px;
		line-height: 30px;
		height:30px;
	}
	.container ul a li span.block {
		float:right;
		height:45px;
	}
	.container ul a li span.glyphicon {
		padding:0 10px;
		color: rgb(100,100,100);
	}
	.container ul a li span.glyphicon-chevron-right {
		font-size:12px;
		float:right;
		height:45px;
		width:25%;
		line-height: 45px;
		color: white;
		padding-right:25px;
	}
	.container ul a li span.glyphicon-plus {
		font-size:12px;
		float:left;
		text-align:left;
		height:45px;
		width:25%;
		line-height: 45px;
		color: white;
		padding-right:25px;
	}


	.container ul a li {
		display: block;
		width: 100%;
		/*margin-bottom:15px;*/
		height: 45px;
		/*border-bottom: 1px solid #b9b9b9;*/
		border-top: 1px solid #5E5E5E;
		background: rgba(0,0,0,.3);
		/*background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#ffffff) to(#ebebeb));*/
		/*background-image: -webkit-linear-gradient(top, #ffffff, #ebebeb);*/
		/*background-image:    -moz-linear-gradient(top, #ffffff, #ebebeb);*/
		/*background-image:      -o-linear-gradient(top, #ffffff, #ebebeb);*/
		/*background-image:         linear-gradient(top, #ffffff, #ebebeb);*/
	}
	.container ul li {
		display: block;
		width: 100%;
		height: 45px;
		line-height: 45px;
		border-top: 1px solid #5E5E5E;
		background: rgba(0,0,0,.3);
	}
	.container ul a li h2 {
		line-height: 45px;
		margin-top: 0;
		margin-bottom: 0;
		font-size: 15px;		
	}
	.container ul a li:hover span h2 {
		color:white;
	}

	.container ul a:hover li h2 { color: #7287b1; }
	
	.container ul a:hover li p.desc { color: #757575; }
	
	.container ul a:hover li span { 
		text-decoration: none;
	}

	.container ul a:hover li {
		color:white;
		background: rgba(0,0,0,.5);
		/*background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#ffffff) to(#efefef));
		background-image: -webkit-linear-gradient(top, #ffffff, #efefef);
		background-image:    -moz-linear-gradient(top, #ffffff, #efefef);
		background-image:      -o-linear-gradient(top, #ffffff, #efefef);
		background-image:         linear-gradient(top, #ffffff, #efefef);*/
		text-transform: none;
	}
	.nopadding {
		padding:0;
	}
	.container ul a li .loc {
		display:inline-block;
		color: rgb(100,100,100);
	}
	.container ul a li .clear {
		clear:both;
	}
	.title_block {
		display: block;
		width: 100%;
		font-size: 12px;
		color:white;
		line-height: 25px;
		padding-left:10px;
		border-top: 1px solid #5E5E5E;
		background: rgba(0,0,0,.6);

	}

	.myblock {
		display: block;
		width: 100%;
		padding:10px;
		color:white;
		margin-bottom: 15px;
		/*height: 45px;*/
		border-top: 1px solid #5E5E5E;
		background: rgba(0,0,0,.3);
	}

	.btn {
		background: rgba(0,0,0,.5);
		border:none;
		box-shadow: none;
		margin-top:5px;
	}

</style>
</head>
<body style= "background: url(<?php echo(base_url());?>assets/img/fondgba.png) 50% 50% / cover no-repeat fixed;">
    <div class="navbar navbar-inverse navbar-default navbar-fixed-top nopadding" style="margin-bottom:0;
    background:rgba(0,0,0,.75);" role="navigation">
      <div class="container" >
        <div class="navbar-header navbar-inverse" style="background-image:none; background-color:transparent;" >
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="<?php echo(base_url());?>index.php/back/home" class="navbar-brand" href="#" style="padding:8px;">
				<?php
				$image_properties = array(
					'src' 		=> 'assets/img/Moi.jpg',
					'class' 	=> 'post_images',
					'width' 	=> '34',
					'height' 	=> '34',
					'title' 	=> 'Utilisateur',
					'rel' 		=> 'lightbox',
				);

				echo img($image_properties);
				?>
			</a>  
			<span style="float:left; color:white; line-height:50px; height:50px;">
				<?php $infos = $this->session->all_userdata();
				//var_dump($this->session->all_userdata());
				echo $infos['user']->prenom;
				echo ' ';
				echo $infos['user']->nom;
				?>
			</span>
        </div>
        <div class="navbar-collapse navbar-inverse collapse" style="background:rgba(0, 0, 0,.5);">
          <ul class="nav navbar-nav">
            <li style="width:auto;"><a href="<?php echo(base_url());?>index.php/back/home">Accueil</a></li>
            <li style="width:auto;"><a href="<?php echo(base_url());?>index.php/back/barcontroller/search">Chercher</a></li>
            <li style="width:auto;"><a href="<?php echo(base_url());?>index.php/back/favoriscontroller/all">Favoris</a></li>
            <li style="width:auto;"><a href="<?php echo(base_url());?>index.php/back/home/logout">Déconnexion</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

	<div class="container" style="margin-top:65px;">

		<style type="text/css">
		.post_images {
			border-radius: 30px;
			border: 1px solid rgba(255, 255, 255, 0.5);
		}

		</style>