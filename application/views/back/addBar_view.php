<?php 
//echo validation_errors();
?>
<span class="title_block">AJOUTER UN BAR</span>
<div class="myblock">
	<form action="" method="post">
		<div class="form-group">
			<label>Nom</label>
			<?php 
			$nom = array(
				'name'    	=> 'nom',
				'id'      	=> 'nom',
				'class'		=> 'form-control',
				'placeholder'	=> 'Son nom',
				'value'		=> set_value('nom')
			);
			echo form_input($nom);
			echo form_error('nom'); 
			?>
		</div>
		<div class="form-group">
			<label>Adresse</label>
			<?php
			$adresse = array(
				'name' 		=> 'adresse',
				'id'		=> 'adresse',
				'class'		=> 'form-control',
				'placeholder'	=> 'Son adresse',
				'value' 	=> set_value('adresse'),
			);
			echo form_input($adresse);
			echo form_error('adresse'); 

			$longitude = array(
				'name'		=> 'longitude',
				'id'		=> 'longitude',
				'class'		=> 'form-control',
				'style' 	=> 'display: none;',
				'value' 	=> set_value('longitude'),
			);
			echo form_input($longitude);

			$latitude = array(
				'name' 		=> 'latitude',
				'id'		=> 'latitude',
				'class'		=> 'form-control',
				'style' 	=> 'display: none;',
				'value' 	=> set_value('latitude'),
			);
			echo form_input($latitude);
			?>
		</div>
		<div class="form-group">
			<label>Tel</label>
			<?php
			$tel = array(
				'name'		=> 'tel',
				'id'		=> 'tel',
				'placeholder'	=> 'Son numéro de tel',
				'class'		=> 'form-control',
				'value' 	=> set_value('tel'),
			);
			echo form_input($tel);
			echo form_error('tel'); 
			?>
		</div>
		<div class="form-group">
			<label>Ville</label>
			<?php
			$ville = array(
				'name' 		=> 'ville',
				'id'		=> 'ville',
				'placeholder'	=> 'Sa ville',
				'class'		=> 'form-control',
				'value'		=> set_value('ville'),
			);
			echo form_input($ville);
			echo form_error('ville');  
			?>
		</div>
		<?php
		echo form_submit('mysubmit', 'Ajouter l\'bar !', 'class="btn btn-primary btn-block"');
	echo form_close();
?>
</div>