			<div class="row">
				<div class="col-md-12">
					<span class="title_block">A PROPOS</span>
					<span class="myblock">
						&copy; 2014 Grenoble Bar App (GBA)<br/>
						Arnaud "Whysper" Roland &amp; Tom "TiDJ" Jamon <br/>
						Projet sous CodeIgniter, Bootstrap, Google Maps v3
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<span class="title_block">TODO</span>
					<span class="myblock">
						- J'aime deviens J'aime plus en AJAX <br/>
						- Système de commentaire simple <br/>
					</span>
				</div>
			</div>
		</div>
		
	</body>
	<script type="text/javascript">
		var base_url = "<?php echo(base_url());?>";
	</script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.2.min.js" ></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/main.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('apigoogle'); ?>&sensor=true"></script>
	<script src="<?php echo base_url()?>assets/js/geolocation.js"></script>  
</html>