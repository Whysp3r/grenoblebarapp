<?php

//var_dump($this->session->userdata('user')->id_user);
//die();
?>
<span class="title_block"><span style="text-transform:uppercase;"><?php echo $bars->nom; ?> </span>(<?php echo $bars->adresse; ?>,<?php echo $bars->ville; ?>)</span>

<div id="bar-details" data-barid="<?php echo $bars->id_bar ?>" data-userid="<?php echo $this->session->userdata('user')->id_user ?>"p class="row">
  <div class="col-sm-12 col-md-12">
    <div class="myblock">
      <?php
        $image_properties = array(
          'src'     => 'assets/img/bars/'.$bars->img,
          'width'   => '100%',
          'height'  => 'auto',
          'alt'     => 'Photo du bar',
          'title'   => 'Photo du bar',
          'rel'     => 'lightbox',
        );
        echo img($image_properties);
        ?>
        <div class="caption">
          <p style="text-align:center;">
            <a href="#" class="btn btn-block btn-primary" role="button">Je m'y accoude</a> 
            <a id="checkin" href="#" class="btn btn-block btn-success" role="button">C'est ma tournée !</a>
            <?php 
            $tel = substr($bars->tel, 1); 
            ?>
            <a class="btn btn-block btn-warning" href="tel:+33<?php echo $tel; ?>"> Appeler la barman</a> 
          </p>
        </div>
    </div>
    <span class="title_block">DERNIER(S) PASSAGE(S)</span>
    <ul style="padding-left:0;">
      <?php 
      foreach ($checks as $check) {
        echo '<li class="nopadding" style="padding-left:10px;white-space:nowrap; overflow:hidden;color:white;">';
        echo '<span class="btn" disabled style="color:white; margin:0 5px 0 0; padding:5px;">';
          ////////////////////////////////////////////////////////////////////////////////////////////
          $date = new DateTime($check->date_passage);
          $now = new DateTime();
          $var1 = $date->format("d");
          $var2 = $now->format("d");
          if(($var1<$var2) || ($var1>$var2)){
            echo $date->format('d/m'); 
          } else {
            echo $date->format('H:i');
          }
          ////////////////////////////////////////////////////////////////////////////////////////////
        echo '</span>';
        echo $check->prenom; 
        echo ' ';
        echo $check->nom;
        echo '<span class="hidden-xs"> y paie une tournée !</span>';
        echo '</li>';
      } 
      ?>

    </ul>
    <span class="title_block">DERNIER(S) COMMENTAIRE(S)</span>
    <ul style="padding-left:0;">
      <?php 
      foreach ($checks as $check) {
        echo '<li class="nopadding" style="line-height:15px; min-height:45px; height:auto; padding-left:10px; color:white;">';
        //echo '<span style="width:45px; height:45px; float:left; display:block; padding:5px 5px 5px 0;">';
        $image_properties = array(
          'src'     => 'assets/img/Moi.jpg',
          'class'   => 'post_images',
          'width'   => '35',
          'height'  => '35',
          'title'   => 'Utilisateur',
          'rel'     => 'lightbox',
        );
        //echo img($image_properties);
        //echo '</span>';
        echo '<span class="btn" disabled style="color:white; margin:0 5px 0 0; padding:5px;">';
          ////////////////////////////////////////////////////////////////////////////////////////////
          $date = new DateTime($check->date_passage);
          $now = new DateTime();
          $var1 = $date->format("d");
          $var2 = $now->format("d");
          if(($var1<$var2) || ($var1>$var2)){
            echo $date->format('d/m'); 
          } else {
            echo $date->format('H:i');
          }
          ////////////////////////////////////////////////////////////////////////////////////////////
        echo ', Par ';
        echo $check->prenom; 
        echo ' ';
        echo $check->nom;
        echo '</span>';
        echo 'Grosse soirée pour ceux qui sont chaud !';
        echo '</li>';
      } 
      ?>

    </ul>
  </div>
</div>


