<!--
<div class="row">	
<div class="col-sm-4 col-md-6 col-lg-8">
<div class="row">
<div class="col-sm-2 col-md-8 col-lg-4" style="background-color:red;">A</div>
<div class="col-sm-2 col-md-2 col-lg-4" style="background-color:blue;">B</div>
<div class="col-sm-8 col-md-2 col-lg-4" style="background-color:green;">C</div>
</div>
</div>
<div class="col-sm-8 col-md-6 col-lg-4" style="background-color:orange;">D</div>
</div>
<i class="glyphicon glyphicon-lock"></i>
<a href="" class="btn btn-warning">Lien</a>
<a href="" class="label label-danger">Lien</a>
<a href="" class="btn btn-primary">Lien</a>
<a href="" class="label label-info">Lien</a>
-->
    
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '318859384936038',
            cookie     : true, 
            xfbml      : true,
            version    : 'v2.0',
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


</script>

<a class="facebookConnect btn btn-info btn-block" href="<?php echo(base_url());?>index.php/front/login/fbr">Se connecter avec Facebook</a><hr>


<div class="row">
	<div class="form-inline col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<form action="" method="post">
			<div class="col-sm-12">
			<!-- <i class="glyphicon glyphicon-envelope"></i>
			<label for="exampleInputEmail1">Adresse email :</label> -->
				<?php
				$email = array(
	              'name'        => 'email',
	              'id'          => 'exampleInputEmail2',
	              'class'		=> 'form-control',
	              'placeholder'	=> 'Ton Adresse Email'
	            );
				echo form_input($email); 
				echo form_error('email'); 
				?>
			</div>
		</div>
	</div>
	<div class="form-inline col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<div class="col-sm-12">
			<!-- <i class="glyphicon glyphicon-lock"></i>
			<label for="exampleInputPassword1">Mot de passe :</label>-->
			<?php
			$pass = array(
						'name' 			=> 'password',
						'type'			=> 'password',
						'id'			=> 'exampleInputPassword1',
						'class'			=> 'form-control',
					    'placeholder'	=> 'Ton Mot de passe'
				);

			echo form_input($pass);
			echo form_error('password'); 
			?>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12">
				<?php
				$submit=array(
					'name'	=> 'mysubmit',
					'type' 	=> 'submit',
					'value'	=> 'Tenter la connection',
					'class'	=> 'btn btn-primary btn-block'
				);
				echo form_submit($submit);
				?>
	</div><hr>
	<div class="col-sm-12 col-md-12 col-lg-12">
		<a class="btn btn-block" href="<?php echo(base_url());?>index.php/front/login/register">S'inscrire a l'ancienne</a><hr>
	</div>
</div>

<style type="text/css">
	
	.btn {
		background-image:none;
		border: none;
	}

</style>