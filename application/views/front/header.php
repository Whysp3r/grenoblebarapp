<html>
<head>
	<title><?php echo $title ?> - OpenBarApp</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css">
</head>
<body>
	<!-- Static navbar -->
  <?php
  $image_properties = array(
    'src'     => 'assets/img/wcm.jpg',
    'alt'     => 'Grenoble Bar App',
    'class'   => 'post_images',
    'width'   => '100%',
    'height'  => 'auto',
    'title'   => 'Welcome in GBA',
  );

  echo img($image_properties);
  ?>

	<div class="container">
    <hr>
    