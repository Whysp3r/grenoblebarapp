
<form action="" method="post">
	<div class="form-group">
		<label>Adresse Email</label>
		<?php 
		$email = array(
			'name'    	=> 'email',
			'id'    	=> 'email',
			'value'		=> set_value('email'),
			'class'		=> 'form-control',
			'type'		=> 'email'
		);
		echo form_input($email);
		echo form_error('email'); 
		?>
	</div>
	<div class="form-group">
		<label>Nom</label>
		<?php 
		$nom = array(
			'name' 		=> 'nom',
			'id'		=> 'nom',
			'value'		=> set_value('nom'),
			'class'		=> 'form-control',
		);

		echo form_input($nom);
		echo form_error('nom'); 
		?>
	</div>
	<div class="form-group">
		<label>Prenom</label>
		<?php 
		$prenom = array(
			'name' 		=> 'prenom',
			'id'		=> 'prenom',
			'value'		=> set_value('prenom'),
			'class'		=> 'form-control',
		);

		echo form_input($prenom);
		echo form_error('prenom'); 
		?>
	</div>
	<div class="form-group">
		<label>Mot de passe</label>
		<?php 
		$pass = array(
			'name' => 'password',
			'id'	=> 'password',
			'type'	=> 'password',
			'class'		=> 'form-control',
		);

		echo form_input($pass);
		echo form_error('password'); 
		?>
	</div>
	<div class="form-group">
		<label>Confirmation Mdp</label>
		<?php 
		$passconf = array(
			'name' 	=> 'passconf',
			'id'		=> 'passconf',
			'type'		=> 'password',
			'class'		=> 'form-control',
		);

		echo form_input($passconf);
		echo form_error('passconf'); 
		?>
	</div>
	

	<?php
	echo form_submit('mysubmit', 'Se declarer alcoolique !', 'class="btn btn-primary btn-block"');
	echo form_close();
	?>