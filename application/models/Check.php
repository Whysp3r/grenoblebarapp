<?php 
class Check extends CI_Model {

	var $user_id;
	var $bar_id; 
	var $date_passage;

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	public function addCheckin($data) {
		$this->db->insert('User_checked_Bar', $data);
	}

	public function getLastCheckin(){

		$query = $this->db->query('SELECT * FROM User_checked_Bar');
		$row = $query->result();
		return $row;
	}
	public function getCheckByIdBar($bar_id) {

		$this->db->select('User.nom, User.prenom, User_checked_Bar.date_passage');
		$this->db->where('User_checked_Bar.Bar_id_bar',$bar_id);
		$this->db->order_by("User_checked_Bar.date_passage", "desc");
		$this->db->join('Bar', 'User_checked_Bar.Bar_id_bar = Bar.id_bar');
		$this->db->join('User','User_checked_Bar.User_id_user = User.id_user');
		
		$query = $this->db->get('User_checked_Bar',5);
		$result = array();

		foreach ($query->result() as $row) {
			$result[]=$row;
		}		
		return $result;
	}

}


?>