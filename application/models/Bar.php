<?php 
class Bar extends CI_Model {

	var $id;
	var $nom; 
	var $adresse;
	var $latitude;
	var $longitude;
	var $tel;
	var $ville;
	var $img;

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	public function getBars() {

		$result = array();
		$query = $this->db->get('Bar');
		/*
		if($query->num_rows()>0){

			foreach($query->result() as $bar){
				$result = $bar;
			}
		}
		*/
		return $query->result();
	}

	function addBar($data){
		$this->db->insert('Bar', $data);
	}

	function getBarById($id){

		$this->db->where('id_bar', $id); 
		$query=$this->db->get('Bar');
		return $query ->row();
	}
	function searchBarByName($name){
		$where = "(nom LIKE '%".$this->db->escape_like_str($name)."%')";
		$this->db->where($where); 
		$query=$this->db->get('Bar');
		return $query ->result();
	}

	

}


?>