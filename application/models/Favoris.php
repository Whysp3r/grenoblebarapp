<?php 
class Favoris extends CI_Model {

	var $user_id;
	var $bar_id; 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	public function addFavoris($data) {
		$this->db->insert('User_like_Bar', $data);
	}

	public function getFavorisByIdUser($user_id) {

		$this->db->select('User.nom, User.prenom, Bar.id_bar, Bar.nom');
		$this->db->where('User_like_Bar.User_id_user',$user_id);

		$this->db->join('Bar', 'User_like_Bar.Bar_id_bar = Bar.id_bar');
		$this->db->join('User','User_like_Bar.User_id_user = User.id_user');
		
		$query = $this->db->get('User_like_Bar');
		$result = array();

		foreach ($query->result() as $row) {
			$result[]=$row;
		}		
		return $result;
	}

}


?>