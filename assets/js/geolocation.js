if(navigator.geolocation) {
  // L'API est disponible
  console.log('Hello Geo');

	function success(pos) {

	  var crd = pos.coords;

	  var lat = crd.latitude;
	  var lon = crd.longitude;

	  console.log('Latitude : ' + lat);
	  console.log('Longitude: ' + lon);

	  $('#longitude').val(crd.longitude);
	  $('#latitude').val(crd.latitude);
	  if($("#map-canvas").length>0){
		initialize(lat,lon);
		}	
	};

	function error(err) {
	  console.warn('ERROR(' + err.code + '): ' + err.message);
	};

	navigator.geolocation.getCurrentPosition(success,error);

} 
else {
  console.log('La géolocalisation est pas activé');
}

function initialize(lat,lon) {

			var myLatLon = new google.maps.LatLng(lat,lon);

	        var mapOptions = {
	          center: myLatLon,
	          zoom: 16
	        };
	        var map = new google.maps.Map(document.getElementById("map-canvas"),
	            mapOptions);

	        if($('#bars_list').length){

		        $.getJSON( base_url+"/index.php/back/Api/bar", function( data ) {
		            $.each( data, function( key, bar) {	
		            	var myLatLon = new google.maps.LatLng(bar.latitude,bar.longitude);
		            	var name = bar.nom;
		            	var adr = bar.adresse;
		             	createMarker(myLatLon,map,name,adr);
		            });                 
		         });    
    		}  
    		else{
    			createMarker(myLatLon,map,null,null);
    		}	       
	     }

function createMarker(myLatLon,map,name,adr){


  var infowindow = new google.maps.InfoWindow({
      content: adr
  });

    var marker = new google.maps.Marker({
	        	position: myLatLon,
	        	map: map,
	        	title:name
	    	});
  google.maps.event.addListener(marker, 'click',function() {
    infowindow.open(map,marker);
  });
}	 





