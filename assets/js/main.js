$( document ).ready(function() {

	if($('#bars_list').length){
        $.getJSON( base_url+"/index.php/back/Api/bar", function( data ) {

            $.each( data, function( key, bar) {

                //visible-xs
                var barLi = "<a href='"+base_url+"/index.php/back/barcontroller/detailBar/"+bar.id_bar+"'><li class='nopadding'>";
                barLi = barLi+"<span class='btitle'><h2>" + bar.nom + "</h2></span>";
                barLi = barLi+"<span class='glyphicon glyphicon-chevron-right'></span>";
                //barLi = barLi+"<span class='hidden-xs bloc'><span class='loc'><span class='glyphicon glyphicon-info-sign'></span>8 Rue du bar  </span><br/>";
                //barLi = barLi+"<span class='glyphicon glyphicon-user loc'></span><span class='loc'>9 Amis  </span><br/>";
               // barLi = barLi+"<span class='loc'><span class='glyphicon glyphicon-map-marker'></span>Localisation  </span><br/>";
                //barLi = barLi+"<span class='loc'><span class='glyphicon glyphicon-comment'></span>87 Commentaires  </span></span></li></a>";
                barLi = barLi+"</li></a>";
                
                $(barLi).appendTo( "#bars_list" );
              
                });    

              var barLi = "<a href='"+base_url+"/index.php/back/barcontroller/addbar'><li class='nopadding'>";
                  barLi = barLi+"<span class='glyphicon glyphicon-plus'></span></span><span class='btitle'><h2> Ajouter Bar</h2></span>";
                  barLi = barLi+"</li></a>";
                $(barLi).appendTo( "#bars_list" );
         });    
    }

    $('#checkin').on('click',function(){

    var idBar = $('#bar-details').data("barid");
    var idUser = $('#bar-details').data("userid");
     
    var $this = $(this);
        $.ajax({
            url:  base_url+"index.php/back/Checkin/",
            type: 'GET',
            data: "idBar="+idBar+"&idUser="+idUser,
            success: function() {
                getCheckIn(idBar);       
            }
        });        
        return false;      
    });

    function getCheckIn(IdBar){

        $.ajax({
            url: base_url+"index.php/back/Checkin/getChecksByIdInJSON/"+IdBar,
            type: "post",
            dataType: 'json',
            success: function(json) {
                $("#checkins").html("");
                json.forEach(function(checkin) {        
                    $("<p><span>"+checkin.prenom+"</span> <span>"+checkin.nom+"</span> a bu un coup à <span>"+checkin.date_passage+"</span></p>").appendTo("#checkins");
                });
            }
        });
    };
});
